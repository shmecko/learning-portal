﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data;
using britelearn.Data.Models;
using britelearn.Data.Interfaces;
using britelearn.Data.Repositories;
using britelearn.ViewModels;


namespace britelearn.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {

        private ICategoryRepository _categoryRepository;
        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View(_categoryRepository.GetAllCategories());
        }
        public IActionResult Create() => View();
        public IActionResult Created(Category category)
        {

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Something went wrong!");
                return View("Create");
            }
            _categoryRepository.AddCategory(category);
            return View();
        }


        public IActionResult Edit(int id) => View(_categoryRepository.GetSingleCategoryById(id));


        public IActionResult Edited(Category newCategory)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Something went wrong!");
                return View("EditCategory", newCategory);
            }
            _categoryRepository.UpdateCategory(newCategory);

            return View();

        }

        public IActionResult Delete(int id) => View(_categoryRepository.CategoryDeletionConfirmation(id));


        public IActionResult Deleted(int id)
        {
            _categoryRepository.DeleteCategories(id);
            return View();
        }

    }
}
