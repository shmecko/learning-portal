﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using britelearn.ViewModels;

namespace britelearn.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITopicRepository _TopicRepository;
        public HomeController(ITopicRepository TopicRepository)
        {
            _TopicRepository = TopicRepository;
        }

        public ViewResult Index()
        {
            var homeViewModel = new HomeViewModel
            {
                FeaturedTopics = _TopicRepository.FeaturedTopics
            };
            return View(homeViewModel);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
