﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using britelearn.ViewModels;
using britelearn.ViewModels.Topic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace britelearn.Controllers
{
    [Authorize]
    public class TopicController : Controller
    {
        private readonly ITopicRepository _TopicRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IExampleRepository _exampleRepository;
        private readonly ICaveatRepository _caveatRepository;

        public TopicController(ITopicRepository TopicRepository, ICategoryRepository categoryRepository, IExampleRepository exampleRepository, ICaveatRepository caveatRepository)
        {
            _TopicRepository = TopicRepository;
            _categoryRepository = categoryRepository;
            _exampleRepository = exampleRepository;
            _caveatRepository = caveatRepository;
        }

        [AllowAnonymous]
        public ViewResult List(string category)
        {
      
            
            string _category = category;
            IEnumerable<Topic> topics;
            string currentCategory = string.Empty;

            if (string.IsNullOrEmpty(category))
            {
                topics = _TopicRepository.GetAllTopic().OrderBy(p => p.TopicId);
                currentCategory = "All";
            }
            else
            {
                //if (string.Equals(category, _category, StringComparison.OrdinalIgnoreCase))
                //{
                    topics = _TopicRepository.GetAllTopic().Where(p => p.Category.CategoryName.Equals(_category)).OrderBy(p => p.Name);
                //}
                //else
                //    topics = _TopicRepository.Topics.Where(p => p.Category.CategoryName.Equals(category)).OrderBy(p => p.Name);

                currentCategory = _category;
            }
             
            return View(new TopicsListViewModel
            {
                Topics = topics,
                CurrentCategory = currentCategory
            });
        }

        [AllowAnonymous]
        public ViewResult Search(string searchString)
        {
            string _searchString = searchString;
            IEnumerable<Topic> Topics;
            string currentCategory = string.Empty;

            if (string.IsNullOrEmpty(_searchString))
            {
                Topics = _TopicRepository.GetAllTopic().OrderBy(p => p.TopicId);
            }
            else
            {
                Topics = _TopicRepository.GetAllTopic().Where(p => p.Name.ToLower().Contains(_searchString.ToLower()));
            }

            return View("~/Views/Topic/List.cshtml", new TopicsListViewModel { Topics = Topics, CurrentCategory = "All Topics" });
        }

        [AllowAnonymous]
        public ViewResult Details(int topicId)
        {
            var topic = _TopicRepository.GetAllTopic().FirstOrDefault(d => d.TopicId == topicId);

            IEnumerable<Example> examples = null;
            IEnumerable<Caveat> caveats = null;

            if (topic != null)
            {
                examples = _exampleRepository.GetExampleByTopicId(topicId).OrderBy(e => e.ExampleOrder);
                caveats = _caveatRepository.GetCaveatByTopicId(topicId);

                TopicViewModel vm = new TopicViewModel
                {
                    TopicId = topic.TopicId,
                    Name = topic.Name,
                    Description = topic.Description,
                    ImageThumbnailUrl = topic.ImageThumbnailUrl,
                    Caveats = caveats,
                    Examples = examples

                };

                return View(vm);
            }

            else
                return View("~/Views/Error/Error.cshtml");


        }


        /** Admin details **/

        [AllowAnonymous]
        public IActionResult Index ()
        {
            return View(_TopicRepository.GetAllTopic());
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Categories = _categoryRepository.GetAllCategories();
            return View();
        }

        [HttpPost]
        public IActionResult Created(TopicCreateViewModel topicView)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Something went wrong");
            
                return View("Create");
            }

            _TopicRepository.AddTopic(topicView);

            return View();
        }
        public IActionResult Edit(int id)
        {
            ViewBag.Categories = _categoryRepository.GetAllCategories();
            return View(_TopicRepository.GetSingleTopicById(id));
        }
        public IActionResult Edited(Topic newTopic)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Something went wrong");
                ViewBag.Categories = _categoryRepository.GetAllCategories();
                return View("Edit", newTopic);
            }
            _TopicRepository.UpdateTopic(newTopic);
            return View();

        }

        public IActionResult Delete(int id) => View(_TopicRepository.GetSingleTopicById(id));

        public IActionResult Deleted(int id)
        {
            _TopicRepository.DeleteTopic(id);

            return View();
        }
        public IActionResult TopicDetails(int id)
        {
            return View(_TopicRepository.TopicDetails(id));
        }
    }
}