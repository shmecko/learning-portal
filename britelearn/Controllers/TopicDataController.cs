﻿using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using britelearn.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace britelearn.Controllers
{
    [Route("api/[controller]")]
    public class TopicDataController : Controller
    {
        private readonly ITopicRepository _TopicRepository;

        public TopicDataController(ITopicRepository TopicRepository)
        {
            _TopicRepository = TopicRepository;
        }

        [HttpGet]
        public IEnumerable<TopicViewModel> LoadMoreTopics()
        {
            IEnumerable<Topic> dbTopics = null;

            dbTopics = _TopicRepository.GetAllTopic().OrderBy(p => p.TopicId).Take(10);

            List<TopicViewModel> Topics = new List<TopicViewModel>();

            foreach (var dbTopic in dbTopics)
            {
                Topics.Add(MapDbTopicToTopicViewModel(dbTopic));
            }
            return Topics;
        }

        private TopicViewModel MapDbTopicToTopicViewModel(Topic dbTopic) => new TopicViewModel()
        {
            TopicId = dbTopic.TopicId,
            Name = dbTopic.Name,
            Description = dbTopic.Description,
            ImageThumbnailUrl = dbTopic.ImageThumbnailUrl
        };

    }
}

