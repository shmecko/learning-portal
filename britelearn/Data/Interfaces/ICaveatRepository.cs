﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;

namespace britelearn.Data.Interfaces
{
    public interface ICaveatRepository
    {
        IEnumerable<Caveat> Caveats { get; }
        IEnumerable<Caveat> GetCaveatByTopicId(int TopicId);
    }
}
