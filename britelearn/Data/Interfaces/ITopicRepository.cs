﻿using britelearn.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.ViewModels;
using britelearn.ViewModels.Topic;

namespace britelearn.Data.Interfaces
{
    public interface ITopicRepository
    {
        IEnumerable<Topic> FeaturedTopics { get; }
        List<Topic> GetAllTopic();
        void AddTopic(TopicCreateViewModel topicDetail);
        Topic GetSingleTopicById(int id);
        void UpdateTopic(Topic newTopic);
        void DeleteTopic(int id);
        List<Example> GetExamplesByTopicId(int topicId);
        Topic TopicDeletionConfirmation(int id);
        TopicViewModel TopicDetails(int id);

    }
}