﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;


namespace britelearn.Data.Interfaces
{
    public interface IExampleRepository
    {
        IEnumerable<Example> Examples { get; }
        IEnumerable<Example> GetExampleByTopicId(int TopicId);
    }
}
