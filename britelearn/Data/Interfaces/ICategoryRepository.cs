﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;

namespace britelearn.Data.Interfaces
{
    public interface ICategoryRepository
    {
        List<Category> GetAllCategories();
        Category GetSingleCategoryById(int id);
        void AddCategory(Category category);
        void UpdateCategory(Category newCategory);
        void DeleteCategories(int id);
        List<Category> GetTopicsByCategoryId(int categoryId);

        Category CategoryDeletionConfirmation(int id);
    }
}

