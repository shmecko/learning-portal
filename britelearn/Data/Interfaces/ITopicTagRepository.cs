﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;

namespace britelearn.Data.Interfaces
{
    public interface ITopicTagRepository
    {
        IEnumerable<Tag> Tags { get; }
        IEnumerable<Topic> GetTopicsByTagId(int TagId);
        IEnumerable<Tag> GetTagsByTopicId (int TopicId);

    }
}
