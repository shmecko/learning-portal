﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using Microsoft.AspNetCore.StaticFiles;

namespace britelearn.Data.Mocks
{
    public class MockCategoryRepository : ICategoryRepository
    {
        public IEnumerable<Category> Categories
        {
            get
            {
                return new List<Category>
                     {
                         new Category { CategoryName = "Database", Description = "Databases & SQL topics" },
                         new Category { CategoryName = "Web Development", Description = "html, css & javascript Topics" }
                     };
            }
        }

        public void AddCategory(Category category)
        {
            throw new NotImplementedException();
        }

        public Category CategoryDeletionConfirmation(int id)
        {
            throw new NotImplementedException();
        }

        public void DeleteCategories(int id)
        {
            throw new NotImplementedException();
        }

        public List<Category> GetAllCategories()
        {
            throw new NotImplementedException();
        }

        public Category GetSingleCategoryById(int id)
        {
            throw new NotImplementedException();
        }

        public List<Category> GetTopicsByCategoryId(int categoryId)
        {
            throw new NotImplementedException();
        }

        public void UpdateCategory(Category newCategory)
        {
            throw new NotImplementedException();
        }
    }
}
