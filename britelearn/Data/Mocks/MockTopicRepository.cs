﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using britelearn.ViewModels;
using britelearn.ViewModels.Topic;
using Microsoft.AspNetCore.StaticFiles;


namespace britelearn.Data.Mocks
{
    public class MockTopicRepository : ITopicRepository
    {
        private readonly ICategoryRepository _categoryRepository = new MockCategoryRepository();

        public IEnumerable<Topic> Topics
        {
            get
            {
                return new List<Topic>
                {
                    new Topic {
                        Name = "Installing SQL Sever",
                        Description = "This is a step by step of how to download, install and configure your sql correctly.",
                        Category = _categoryRepository.GetAllCategories().First(),
                        IsFeaturedTopic = true,
                        ImageUrl = "Placeholder.jpg",
                        ImageThumbnailUrl = "Placeholder.jpg"
                    },
                    new Topic {
                        Name = "Database tools",
                        Description = "This topic explains tools to use for database",
                        Category =  _categoryRepository.GetAllCategories().First(),
                        IsFeaturedTopic = false,
                        ImageUrl = "Placeholder.jpg",
                        ImageThumbnailUrl = "Placeholder.jpg"
                    },
                    new Topic {
                        Name = "Quering Data with T-SQL",
                        Description = "this topic show user how to querry data with T-SQL",
                        Category =  _categoryRepository.GetAllCategories().First(),
                        IsFeaturedTopic = false,
                        ImageUrl = "Placeholder.jpg",
                        ImageThumbnailUrl = "Placeholder.jpg"
                    },
                    new Topic
                    {
                        Name = "Location with HTML5",
                        Description = "This project shows how to work with location on HTML5",
                        Category = _categoryRepository.GetAllCategories().Last(),
                        IsFeaturedTopic = false,
                        ImageUrl = "Placeholder.jpg",
                        ImageThumbnailUrl = "Placeholder.jpg"
                    }
                };
            }
        }
        public IEnumerable <Topic> FeaturedTopics { get; }

        public void AddTopic(TopicCreateViewModel topicCreate)
        {
            throw new NotImplementedException();
        }

        public void DeleteTopic(int id)
        {
            throw new NotImplementedException();
        }

        public List<Topic> GetAllTopic()
        {
            throw new NotImplementedException();
        }

        public List<Example> GetExamplesByTopicId(int topicId)
        {
            throw new NotImplementedException();
        }

        public Topic GetSingleTopicById(int id)
        {
            throw new NotImplementedException();
        }

        public Topic GetTopicById(int TopicId)
        {
            throw new NotImplementedException();
        }

        public Topic TopicDeletionConfirmation(int id)
        {
            throw new NotImplementedException();
        }

        public TopicViewModel TopicDetails(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateTopic(Topic newTopic)
        {
            throw new NotImplementedException();
        }
    }
}
