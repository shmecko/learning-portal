﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace britelearn.Data.Repositories
{
    public class ExampleRepository : IExampleRepository
    {
        private readonly AppDbContext _appDbContext;
        public ExampleRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Example> Examples => _appDbContext.Examples.AsQueryable();

        public IEnumerable<Example> GetExampleByTopicId(int topicId) 
            => _appDbContext.Examples.AsQueryable().ToList().Where(e => e.TopicId == topicId);
        


    }

}

