﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace britelearn.Data.Repositories
{
    public class CaveatRepository : ICaveatRepository
    {
        private readonly AppDbContext _appDbContext;
        public CaveatRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Caveat> Caveats => _appDbContext.Caveats.AsQueryable();

        public IEnumerable<Caveat> GetCaveatByTopicId(int topicId)
            => _appDbContext.Caveats.AsQueryable().ToList().Where(e => e.TopicId == topicId);

    }
}
