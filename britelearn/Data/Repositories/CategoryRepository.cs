﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;


namespace britelearn.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _appDbContext;
        public CategoryRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        public List<Category> GetAllCategories()
        {
            return _appDbContext.Categories.AsQueryable().ToList();
        }
        

        //Insert
        public void AddCategory(Category category)
        {
            _appDbContext.Categories.Add(category);
            _appDbContext.SaveChanges();
        }

        //Delete
        public void DeleteCategories(int id)
        {
            Category categoryToBeDeleted = GetSingleCategoryById(id);
            _appDbContext.Categories.Remove(categoryToBeDeleted);
            _appDbContext.SaveChanges();
        }

        //retrieve single category
        public Category GetSingleCategoryById(int id) => _appDbContext.Categories.Where(n => n.CategoryId == id).FirstOrDefault();

        //retrieve category topics
        public List<Category> GetTopicsByCategoryId(int categoryId) => _appDbContext.Categories.Where(n => n.CategoryId == categoryId).ToList();


        //update category
        public void UpdateCategory(Category newCategory)
        {
            Category oldCategory = GetSingleCategoryById(newCategory.CategoryId);
            oldCategory.CategoryName = newCategory.CategoryName;
            oldCategory.Description = newCategory.Description;
            oldCategory.Topics = newCategory.Topics;

            _appDbContext.SaveChanges();
        }

        public Category CategoryDeletionConfirmation(int id)
        {

            return GetSingleCategoryById(id);

        }
    }
}
