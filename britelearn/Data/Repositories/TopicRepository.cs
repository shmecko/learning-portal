﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using britelearn.ViewModels.Topic;
using britelearn.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace britelearn.Data.Repositories
{
    public class TopicRepository : ITopicRepository
    {

        private readonly AppDbContext _appDbContext;
        public TopicRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }



        public IEnumerable<Topic> FeaturedTopics => _appDbContext.Topics.AsQueryable().Where(p => p.IsFeaturedTopic).Include(c => c.Category);

        public void AddTopic(TopicCreateViewModel topicDetail)
        {
            Topic newTopic = new Topic();
            newTopic.Name = topicDetail.Name;
            newTopic.Description = topicDetail.Description;
            newTopic.IsFeaturedTopic = topicDetail.IsFeaturedTopic;
            newTopic.CategoryId = topicDetail.CategoryId;
            newTopic.ImageUrl = topicDetail.ImageUrl;
            newTopic.ImageThumbnailUrl = topicDetail.ImageThumbnailUrl;
            _appDbContext.SaveChanges();


            //if(topicDetail.Caveats.Count() != 0)
            if (topicDetail.Examples != null)
            {
                foreach (var item in topicDetail.Examples)
                {
                    Example newExample = new Example();
                    newExample.ExampleHeading = item.ExampleHeading;
                    newExample.ExampleText = item.ExampleText;
                    newExample.ExampleOrder = item.ExampleOrder;
                    newExample.TopicId = item.TopicId;
                    _appDbContext.SaveChanges();
                }
            }

            if (topicDetail.Caveats != null)
            {

                foreach (var item in topicDetail.Caveats)
                {
                    Caveat newCaveat = new Caveat();
                    newCaveat.CaveatName = item.CaveatName;
                    newCaveat.CaveatDiscription = item.CaveatDiscription;
                    newCaveat.TopicId = item.TopicId;
                    _appDbContext.SaveChanges();
                }
            }

        }

        public void DeleteTopic(int id)
        {
            try
            {


                Topic topicToBeDeleted = GetSingleTopicById(id);
                _appDbContext.Topics.Remove(topicToBeDeleted);
                _appDbContext.SaveChanges();
            }
            finally
            {
                
            }
        }

        public List<Topic> GetAllTopic()
        {
            List<Topic> topics = _appDbContext.Topics.AsQueryable().Include(c => c.Category).ToList();
            return topics;
        }

        public List<Example> GetExamplesByTopicId(int topicId)
        {
            List<Example> examples = _appDbContext.Examples.Where(n => n.TopicId == topicId).ToList();
            return examples;
        }

        public List<Caveat> GetCaveatsByTopicId(int topicId)
        {
            List<Caveat> caveats = _appDbContext.Caveats.Where(n => n.TopicId == topicId).ToList();
            return caveats;
        }

        public Topic GetSingleTopicById(int id) => _appDbContext.Topics.AsQueryable().FirstOrDefault(p => p.TopicId == id);
     

        public Topic TopicDeletionConfirmation(int id)
        {
            return GetSingleTopicById(id);
        }

        public TopicViewModel TopicDetails(int id)
        {
            Topic topic = GetSingleTopicById(id);
            TopicViewModel topicVM = new TopicViewModel()
            {
                TopicId = topic.TopicId,
                Name = topic.Name,
                Description = topic.Description,
                ImageThumbnailUrl = topic.ImageThumbnailUrl,
                Caveats = GetCaveatsByTopicId(id),
                Examples = GetExamplesByTopicId(id)
            };
            return topicVM;
        }

        public void UpdateTopic(Topic newTopic)
        {
            Topic oldTopic = GetSingleTopicById(newTopic.TopicId);

            oldTopic.Name = newTopic.Name;
            oldTopic.Description = newTopic.Description;
            oldTopic.IsFeaturedTopic = newTopic.IsFeaturedTopic;
            oldTopic.CategoryId = newTopic.CategoryId;
            oldTopic.ImageUrl = newTopic.ImageUrl;
            oldTopic.ImageThumbnailUrl = newTopic.ImageThumbnailUrl;
           

            _appDbContext.SaveChanges();
        }
    }
}
