﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Interfaces;
using britelearn.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace britelearn.Data.Repositories
{
    public class TopicTagRepository : ITopicTagRepository
    {
        public IEnumerable<Tag> Tags => throw new NotImplementedException();

        public IEnumerable<Tag> GetTagsByTopicId(int TopicId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Topic> GetTopicsByTagId(int TagId)
        {
            throw new NotImplementedException();
        }
    }
}
