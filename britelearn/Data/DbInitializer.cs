﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;


namespace britelearn.Data.Models
{
    public class DbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            AppDbContext context =
                applicationBuilder.ApplicationServices.GetRequiredService<AppDbContext>();

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(Categories.Select(c => c.Value));
            }

            if (!context.Topics.Any())
            {
                context.AddRange
                (
                    new Topic
                    {
                        Name = "Topic 1",
                        Description = "Description for topic 1",
                        Category = Categories["Database"],
                        ImageUrl = "http://imgh.us/beerL_2.jpg",

                        IsFeaturedTopic = true,
                        ImageThumbnailUrl = "https://weneedfun.com/wp-content/uploads/2016/07/Girls-Black-and-White-Profile-Pictures-8.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 2",
                        Description = "Description for topic 2",
                        Category = Categories["Database"],
                        ImageUrl = "http://imgh.us/rumCokeL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/rumAndCokeS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 3",
                        Description = "Description for topic 3",
                        Category = Categories["Database"],
                        ImageUrl = "http://imgh.us/tequilaL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/tequilaS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 4",
                        Description = "Description for topic 4",
                        Category = Categories["Database"],
                        ImageUrl = "http://imgh.us/wineL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/wineS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 5",
                        Category = Categories["Database"],
                        Description = "Description for topic 5",
                        ImageUrl = "http://imgh.us/margaritaL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/margaritaS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 6",
                        Description = "Description for topic 6",
                        Category = Categories["Database"],
                        ImageUrl = "C:/Users/machilane.mohlala/Source/Repos/learning-portal/britelearn/wwwroot/images/Placeholder.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/whiskeyS.jpg"
                    },

                    new Topic
                    {
                        Name = "Topic 7",
                        Description = " Description for topic 7",
                        Category = Categories["Web Development"],
                        ImageUrl = "http://imgh.us/coffeeL.jpg",
                        IsFeaturedTopic = true,
                        ImageThumbnailUrl = "http://imgh.us/coffeS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 8",
                        Description = "Description for topic 8",
                        Category = Categories["Web Development"],
                        ImageUrl = "http://imgh.us/kvassL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/kvassS.jpg"
                    },
                    new Topic
                    {
                        Name = "Topic 9",
                        Description = "Description for topic 9",
                        Category = Categories["Web Development"],
                        ImageUrl = "http://imgh.us/juiceL.jpg",
                        IsFeaturedTopic = false,
                        ImageThumbnailUrl = "http://imgh.us/juiceS.jpg"
                    }
                );
            }

            context.SaveChanges();

            //var examples = new Example[]
            //{
            //new Example{

            //    ExampleID=1,
            //    ExampleHeading= "Heading 1 for Topic 1",
            //    ExampleText="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //    ExampleOrder = 1,
            //    TopicId = 1 },

            //new Example{

            //    ExampleID=2,
            //    ExampleHeading= "Heading 1 for Topic 1",
            //    ExampleText="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //    ExampleOrder = 1,
            //    TopicId = 1 },

            //new Example{

            //    ExampleID=3,
            //    ExampleHeading= "Heading 2 for Topic 1",
            //    ExampleText="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //    ExampleOrder = 2,
            //    TopicId = 1 },

            //new Example{

            //    ExampleID=4,
            //    ExampleHeading= "Heading 1 for Topic 2",
            //    ExampleText="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //    ExampleOrder = 1,
            //    TopicId = 2 },

            //new Example{

            //    ExampleID=5,
            //    ExampleHeading= "Heading 2 for Topic 1",
            //    ExampleText="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            //    ExampleOrder = 2,
            //    TopicId = 1 },

            //};

            //foreach (Example c in examples)
            //{
            //    context.Examples.Add(c);
            //}
            //context.SaveChanges();
        }

        private static Dictionary<string, Category> categories;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (categories == null)
                {
                    var genresList = new Category[]
                    {
                        new Category { CategoryName = "Database", Description="All database Topics" },
                        new Category { CategoryName = "Web Development", Description="All web development Topics" }
                    };

                    categories = new Dictionary<string, Category>();

                    foreach (Category genre in genresList)
                    {
                        categories.Add(genre.CategoryName, genre);
                    }
                }

                return categories;
            }
        }
    }
}
