﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace britelearn.Data.Models
{
    public class TopicTag
    {


        public int TagId { get; set; }
        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }



        //public int TopicId { get; set; }
        //[ForeignKey("TopicId")]
        //public virtual Topic Topic { get; set; }
    }
}
