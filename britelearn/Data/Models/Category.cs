﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace britelearn.Data.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        
        [Required(ErrorMessage = "This field is required")]
        [StringLength(50, ErrorMessage = "The category name is too long.")]
        public string CategoryName { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "The category description is too long.")]
        public string Description { get; set; }


        //Entity Relations
        public List<Topic> Topics { get; set; }
    }
}
