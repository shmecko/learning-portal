﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace britelearn.Data.Models
{
    public class Example
    {

        [Key]
        public int ExampleID { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage ="This field is required")]
        public string ExampleHeading { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "This field is required")]
        public string ExampleText { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int ExampleOrder { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int TopicId { get; set; }
        [ForeignKey("TopicId")]
        public virtual Topic Topic { get; set; }
    }
}
