﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace britelearn.Data.Models
{
    public class Enroll
    {
        [Key]
        public int EnrollId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public DateTime EnrollDate { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int TopicId { get; set; }
        [ForeignKey("TopicId")]
        public virtual Topic Topic { get; set; }

    }
}
