﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace britelearn.Data.Models
{
    public class Caveat
    {
        [Key]
        public int CaveatID { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "This field is required")]
        public string CaveatName { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "This field is required")]
        public string CaveatDiscription { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int TopicId { get; set; }
        [ForeignKey("TopicId")]
        public virtual Topic Topic { get; set; }

    }
}
