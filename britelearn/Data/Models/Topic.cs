﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace britelearn.Data.Models
{
    public class Topic
    {
        [Key]
        public int TopicId { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage ="Topic name field is required")]
        [StringLength(20, ErrorMessage = "The name is too long.")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Topic Description field is required")]
        [StringLength(500, ErrorMessage = "The topic description is too long.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public bool IsFeaturedTopic { get; set; }


        [Required(ErrorMessage = "This field is required")]
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ImageThumbnailUrl { get; set; }

        public List<Example> Examples { get; set; }
        public List<Caveat> Caveats { get; set; }
        
    }
}
