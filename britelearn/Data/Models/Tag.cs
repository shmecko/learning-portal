﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace britelearn.Data.Models
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }

        [DataType(DataType.Text)]
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }

        public List<TopicTag> TopicTags { get; set; }
    }
}
