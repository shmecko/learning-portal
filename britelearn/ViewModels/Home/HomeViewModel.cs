﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;


namespace britelearn.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<britelearn.Data.Models.Topic> FeaturedTopics { get; set; }
    }
}
 