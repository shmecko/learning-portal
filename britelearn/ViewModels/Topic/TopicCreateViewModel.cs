﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;

namespace britelearn.ViewModels.Topic
{
    public class TopicCreateViewModel
    {

        public int TopicId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsFeaturedTopic { get; set; }
        public int CategoryId { get; set; }
        public string ImageUrl { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public IEnumerable<Caveat> Caveats { get; set; }
        public IEnumerable<Example> Examples { get; set; }
    }
}
