﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using britelearn.Data.Models;

namespace britelearn.ViewModels
{
    public class TopicsListViewModel
    {
        public IEnumerable<britelearn.Data.Models.Topic> Topics { get; set; }
        public string CurrentCategory { get; set; }
    }
}
